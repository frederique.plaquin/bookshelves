import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore'
import { AuthService } from "../services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isAuth!: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged(
      (user) => {
        // revient à écrire
        // if(user) {
        //this.isAuth = true;
        // } else {
        // this.isAuth = false;
        // }
        this.isAuth = !!user;
      }
    );
  }

  onSignOut() {
    this.authService.signOutUser();
  }

}
