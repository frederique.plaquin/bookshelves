import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BooksService } from "../../services/books.service";
import { Router } from "@angular/router";
import { Book } from "../../models/Book.model";

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {

  bookForm!: FormGroup; // formulaire bookForm de type FormGroup
  fileIsUploading= false; // pour savoir si il y a un fichier entrain de charger
  fileUrl!: string; // pr récupérer l url du fichier
  fileUploaded = false; // pr signaler fin du chargement du fichier

  constructor(private formBuilder: FormBuilder,
              private booksService: BooksService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required]
    });
  }

  onSaveBook() {
    const title = this.bookForm.get('title')?.value;
    const author = this.bookForm.get('author')?.value;
    const newBook = new Book(title, author);
    if(this.fileUrl && this.fileUrl !== '') { // si une photo a été uploadée
      newBook.photo = this.fileUrl; // alors on définit la photo du nv book avec l'url du fichier uploadé
    }
    this.booksService.createNewBook(newBook);
    this.router.navigate(['/books']);
  }

  onUploadFile(file: File) { // va déclencher méthode du service et mettre à jour DOM au fur et à mesure
    this.fileIsUploading = true; // on déclare qu un fichier est entrain d'uploader
    this.booksService.uploadFile(file).then( // on lance la méthode de chargement du fichier
      (url: any) => { // on récupèrera un url en réponse
        this.fileUrl = url; // on enregistre url ds variable
        this.fileIsUploading = false; // on déclare que le chargement du fichier est terminé
        this.fileUploaded = true; // le téléchargement du fichier d'est termniné avec succès
      }
    )
  }

    detectFiles(event: any) {
      this.onUploadFile(event.target.files[0]); // on déclenche onUploadFile avec le fichier de l input
    }

}
