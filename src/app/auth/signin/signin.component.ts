import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {


  signInForm!: FormGroup;
  errorMessage!: string;

  constructor(private formBuilder: FormBuilder, // pour construction formulaire
              private authService: AuthService, // pour services d'authentification
              private router: Router) { } // pour services du router

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.signInForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]] // pattern prend comme arg une expression regulière (regex) ici obligerl tuilisateur a entrer au moins 6 caractères de type alphanumérique comme demandé par firebase
    });
  }

  onSubmit() {
    const email = this.signInForm.get('email')?.value;
    const password = this.signInForm.get('password')?.value;
    this.authService.signInUser(email,password).then( // ici on doit respecter l'async de createNewUser()
      () => {
        this.router.navigate(['/books']); // si ok on redirige vers la liste de livres view
      },
      (error) => {
        this.errorMessage = error; // on injecte le message d'erreur ds le template
      }
    );
  }
}
