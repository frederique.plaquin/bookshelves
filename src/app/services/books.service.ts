import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { Book } from "../models/Book.model";
import firebase from "firebase/compat/app";
import "firebase/compat/database"; // "firebase/compat/database" et nn "firebase/database"
import 'firebase/compat/storage';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books: Book[] = [];
  bookSubject = new Subject<Book[]>();

  constructor() { }

  emitBooks() {
    this.bookSubject.next(this.books); // prend le contenu de l'array books et emet le subject
  }

  saveBooks() { // méthode pour enregistrer books ds bdd
    firebase.database().ref('/books').set(this.books); // on créé le node /books -> tous les livres seront stockés ds ce node fonctionne un peu comme put en php -> écrase données préexistantes ds /books
  }

  getBooks() { // méthode pour récupérer les livres
    firebase.database().ref('/books')
      .on('value', (data) => {
        this.books = data.val() ? data.val() : []; // operateur terniare '?' permet de dire que variable peut être undefined
        this.emitBooks();
      }) // .on() permet de réagir aux modifications de la bdd
  }

  getSingleBook(id: number) { // méthode pour récupérer un seul livre ds la bdd
    return new Promise(
      ((resolve, reject) => {
        firebase.database().ref('/books/' + id).once('value').then( // on utilise once() car on a bvesoin de retourner les données qu'une fois contrairement à on() de getBooks()
          (data) => {
            resolve(data.val()); // ici si résultat data.val va se remplir avec la valeur de la donnée
          },
          (error) => {
            reject(error); // sinon on reject avec erreur en argument
          }
        )
      })
    )
  }

  createNewBook(newBook: Book) {
    this.books.push(newBook); // on ajoute le nv livre à l'array des books
    this.saveBooks(); // on enregistre les données sur le serveurs (l array books)
    this.emitBooks(); // puis on émet le subject
  }

  removeBook(book: Book) { // méthode pour supprimer un livre de la liste
    if(book.photo) { // si le livre a une photo
      const storageRef = firebase.storage().refFromURL(book.photo); // on recupere l'url de reference du fichier photo
      storageRef.delete().then(// on la supprime du firebase storage et de la bdd
        () => {
          console.log('Photo supprimée !');
        }
      ).catch( // au cas ou la suppressiond  image ne fonctionne pas
        (error) => {
          console.log('Fichier non trouvé' + error)
        }
      )
    }
    const bookIndexToRemove = this.books.findIndex(
      (bookEl) => { // on parcourt l'array book
        // revient à écrire         if(bookEl === book) { // si le book de l array est le même que le book envoyé
        //                            return true; // élément livre est passé en argument
        //                          } else {
        //                             return false;
        //                          }
        return bookEl === book;
      }
    );
    this.books.splice(bookIndexToRemove, 1); // on supprime le livre de l'array
    this.saveBooks(); // on enregistre les données sur le serveurs (l array books)
    this.emitBooks(); // puis on émet le subject
  }

  uploadFile(file: File) { // méthode asynchrone car uploader un fichier prend du temps
    return new Promise(
      ((resolve, reject) => {
        const almostUniqueFileName = Date.now().toString(); // on génère un nom de fichier unique basé sur timestamp
        const upload = firebase.storage().ref()// on stock l'upload ds variable ; ref() donne référence à la racine du storage
          .child('images/' + almostUniqueFileName + file.name) // on défini path de l'upload
          .put(file); // on enregistre le fichier dans le dossier au nom de fichier choisit comme sous dossier de la racine du storage
        const storage = firebase.storage();
        upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
          () => {
            console.log('Chargement...');
          },
          (error) => {
            console.log('Erreur de chargement !' + error);
            reject(); // on reject la promise car erreur
          },
          () => {
            storage.ref('images/' + almostUniqueFileName + file.name).getDownloadURL().then( // get url from snapshot
              (url) => {
                resolve(url); // downloadURL  recupere url de l image ds le storage afin de pv l enregistrer ds bdd
              }
            )
          }
        ); // on réagit à chaque changement d'état du téléchargement
      })
    );
  }
}
