import { Component } from '@angular/core';
// Import the functions you need from the SDKs you need

// Your web app's Firebase configuration
// Import the functions you need from the SDKs you need
import { getFirestore} from 'firebase/firestore/lite';

import firebase from "firebase/compat/app";
import "firebase/compat/database"; // "firebase/compat/database" et nn "firebase/database"
import 'firebase/compat/storage';

// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
// Your web app's Firebase configuration

    const firebaseConfig = {

      apiKey: "AIzaSyCLfsaVqGSKQM2_9FzzQk1tBHsXs0j16kQ",
      authDomain: "bookshelves-1f26f.firebaseapp.com",
      projectId: "bookshelves-1f26f",
      databaseURL: 'https://bookshelves-1f26f-default-rtdb.europe-west1.firebasedatabase.app',
      storageBucket: "bookshelves-1f26f.appspot.com",
      messagingSenderId: "854029644092",
      appId: "1:854029644092:web:905ac7f5992a00e3ecae39"
    };


// Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    const db = getFirestore();
  }
}
