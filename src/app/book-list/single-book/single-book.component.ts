import { Component, OnInit } from '@angular/core';
import { Book } from "../../models/Book.model";
import { ActivatedRoute, Router } from "@angular/router";
import { BooksService } from "../../services/books.service";


@Component({
  selector: 'app-single-book',
  templateUrl: './single-book.component.html',
  styleUrls: ['./single-book.component.scss']
})
export class SingleBookComponent implements OnInit {

  book!: Book; // objet de type book

  constructor(private route: ActivatedRoute,
              private booksService: BooksService,
              private router: Router) { } // injection de activaterRoute pr recup l'id de l'url + booksService + router

  ngOnInit(): void {
    this.book = new Book('',''); // creation d'un book vide temporaire pr eviter les erreurs si données prenennt trop de temps à arriver lors du chargement de la page
    const id = this.route.snapshot.params['id'];// recuperation de l'id du book depuis l url
    this.booksService.getSingleBook(+id).then(
      (book: any) => { // any à la place de book: Book signon erreur type unknown is not assignable to type void
        this.book = book;
      }
    ); // récupération des données du book correspondant à l'id obtenu ('+id' pour cast l'id antant que number et non string)
  }

  onBack() { // méthode retour page view list books
    this.router.navigate(['/books']);
  }

}
