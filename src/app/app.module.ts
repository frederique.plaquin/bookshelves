import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { BookListComponent } from './book-list/book-list.component';
import { SingleBookComponent } from './book-list/single-book/single-book.component';
import { BookFormComponent } from './book-list/book-form/book-form.component';
import { HeaderComponent } from './header/header.component';
import { AuthService } from "./services/auth.service"; // s'importe automatiquement lors de l ajout du service à providers
import { BooksService } from "./services/books.service"; // s'importe automatiquement lors de l ajout du service à providers
import { AuthGuardService } from "./services/auth-guard.service"; // s'importe automatiquement lors de l ajout du service à providers
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; // s'importe automatiquement lors de l ajout du module à imports
import { HttpClientModule } from "@angular/common/http"; // s'importe automatiquement lors de l ajout du module à imports
import { RouterModule, Routes } from "@angular/router"; // s'importe automatiquement lors de l ajout du module à imports

const appRoutes: Routes = [
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/signin', component: SigninComponent },
  { path: 'books', canActivate: [AuthGuardService] , component: BookListComponent },
  { path: 'books/new', canActivate: [AuthGuardService] , component: BookFormComponent },
  { path: 'books/view/:id', canActivate: [AuthGuardService] , component: SingleBookComponent },
  { path: '', redirectTo: 'books', pathMatch: 'full' }, // affichage par default si rien n'est spécifié ds l url + pathMatch: 'full' évite erreurs de redirection
  { path: '**', redirectTo: 'books' } // path wildcard tjs à placer à la fin c'est l'affichage par default si rien url incorrect
]

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    BookListComponent,
    SingleBookComponent,
    BookFormComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    BooksService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
