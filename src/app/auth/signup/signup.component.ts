import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms"; // s'importe auto qd on ajoute FormGroup, FormBuilder ds class
import { AuthService } from "../../services/auth.service"; // s'importe auto
import { Router } from "@angular/router"; // s'importe auto


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signUpForm!: FormGroup;
  errorMessage!: string;

  constructor(private formBuilder: FormBuilder, // pour construction formulaire
              private authService: AuthService, // pour services d'authentification
              private router: Router) { } // pour services du router

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.signUpForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]] // pattern prend comme arg une expression regulière (regex) ici obligerl tuilisateur a entrer au moins 6 caractères de type alphanumérique comme demandé par firebase
    });
  }

  onSubmit() {
    const email = this.signUpForm.get('email')?.value;
    const password = this.signUpForm.get('password')?.value;
    this.authService.createNewUser(email,password).then( // ici on doit respecter l'async de createNewUser()
      () => {
        this.router.navigate(['/books']); // si ok on redirige vers la liste de livres view
      },
      (error) => {
        this.errorMessage = error; // on injecte le message d'erreur ds le template
      }
    );
  }

}
