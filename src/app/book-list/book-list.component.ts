import { Component, OnDestroy, OnInit } from '@angular/core';
import { Book } from "../models/Book.model";
import { Subscription } from "rxjs";
import { BooksService } from "../services/books.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {

  books!: Book[]; // creationb de l'array local books
  booksSubscription!: Subscription; // souscription au subject

  constructor(private booksService: BooksService,
              private router: Router) { }

  ngOnInit(): void {
    this.booksSubscription = this.booksService.bookSubject.subscribe( // on souscrit au sujet
      (books:Book[]) => {
        this.books = books; // on récupère un array de type Book
      }
    );
    this.booksService.getBooks(); // on récupère la liste de livres enregistrés ds bdd
    this.booksService.emitBooks(); // on emet le subject pour la première fois
  }

  onNewBook() { // qd on clique sur ajouter un livre on redirige vers formulaire d'ajout de livre
    this.router.navigate(['/books', 'new']);
  }

  onDeleteBook(book: Book) { // on appel méthode de suppression de livvre de book.service.ts
    this.booksService.removeBook(book);
  }

  onViewBook(id: number) { // redirection vers la page d'un livre unique
    this.router.navigate(['/books', 'view', id]);
  }

  ngOnDestroy() {
    this.booksSubscription.unsubscribe();
  }

}
